#include <iostream>

#include "Bat.h"
#include "Ball.h"
#include <sstream>
#include <cstdlib>
#include <SFML/Graphics.hpp>

int main() {
    // Create a video mode object
    sf::VideoMode vm{1920, 1080};
    // Create and open window
    sf::RenderWindow window{vm, "pong", sf::Style::Fullscreen};

    // global scope variables
    int score = 0;
    int lives = 3;
    // Create a bat at the bottom center of the screen
    Bat bat{1920 / 2, 1080 - 20};
    // Create a ball
    Ball ball{1920 / 2, 0};
    // Create a text object
    sf::Text hud;
    // use asset font
    sf::Font font;
    font.loadFromFile("assets/font/Computerfont.ttf");
    // Set font of the hud
    hud.setFont(font);
    hud.setCharacterSize(75);
    hud.setFillColor(sf::Color::White);
    hud.setPosition(20, 20);
    // game Clock
    sf::Clock clock;
    while (window.isOpen()) {
        // Handle player input TODO
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            bat.moveRight();
        } else {
            bat.stopRight();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            bat.moveLeft();
        } else {
            bat.stopLeft();
        }
        // Update the scene TODO
        sf::Time dt = clock.restart();
        bat.update(dt);
        ball.update(dt);
        std::stringstream ss;
        ss << "Score:" << score << "  Lives:" << lives;
        hud.setString(ss.str());
        // Handle ball rebound
        // Ball hitting bottom
        if (ball.getPosition().top > window.getSize().y) {
            // Reverse ball direction
            ball.reboundBottom();
            // Remove a life
            lives--;
            // Check for zero lives
            if (lives == 0) {
                score = 0;
                lives = 3;
                ball.resetSpeed();
            }
        }
        // Ball hitting top
        if (ball.getPosition().top < 0) {
            ball.reboundBatOrTop();
            ball.speedUp();
            score++;
        }
        // Ball hitting sides
        if (ball.getPosition().left <= 0 || ball.getPosition().left + ball.getPosition().width >= window.getSize().x) {
            ball.reboundSides();
        }
        // Ball hitting the bat
        if (ball.getPosition().intersects(bat.getPosition())) {
            ball.reboundBatOrTop();
        }
        // Draw the scene TODO
        window.clear();
        window.draw(hud);
        window.draw(bat.getShape());
        window.draw(ball.getShape());
        window.display();
    }
    return 0;
}
